1. Open the file ./crawler/_config.php in an editor and edit the 
   MySQL config lines and $CRAWL_ENTRY_POINT_URL to match your settings.
   DO NOT FORGET to put http:// prefix!!!

2. Create the necessary database tables.  Make sure you have created 
   the database ahead of time. You should know your mysql username and
   password as well.

   Get into the search/crawler directory and run the following command:

    mysql -u<username> -p<password> <databasename> < crawler.sql

   this will create phpcrawler_links table.

   You could also cut and paste the sql statements from crawler.sql into a 
   package like phpMyAdmin or into your terminal window.

3. Run crawler via the web search/crawler/crawler.php or using command line

    php -q crawler.php
