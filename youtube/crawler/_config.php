<?php
/**
 * <h1>PHP Crawler</h1>
 */

if (empty($GLOBALS["www_has_crawl_config"])) {
// We both know about reqire_once(), I just keep old style.
$GLOBALS["www_has_crawl_config"] = 1;


// *** MySQL database config. Please change these lines according your host
$s_op['mysql_host'] = "localhost";
$s_op['mysql_db'] = "phpcrawl";
$s_op['mysql_user'] = "root";
$s_op['mysql_pass'] = "";

//$CRAWL_ENTRY_POINT_URL = "http://".$_SERVER['HTTP_HOST']; // website to crawl MUST begins with http:// prefix
$CRAWL_ENTRY_POINT_URL = "http://www.youtube.com";

$CRAWL_LOCALE = "en_US"; // read more about Locate http://php.rinet.ru/manual/en/function.setlocale.php
//$CRAWL_LOCALE = "ru_RU";

$CRAWL_MAX_DEPTH = 3;   // PHP Crawler uses recursive retrieving. Specify recursion maximum depth level.
$CRAWL_PAGE_EXPIRE_DAYS = 10; // Page reindex period

// **** MISC SETTINGS ****

// disable keys while crawling (might save some time)
$CRAWL_DB_DISABLE_KEYS = false;

// skip crawling long URLs
$CRAWL_URL_MAX_LEN = 1024; // default 1024

// allow crawling these extensions possible to find in any href="" attributes (<link /> or <a>)
$CRAWL_ALLOW_EXT = array('html', 'txt', 'htm', 'xhtml', 'php', 'asp');

// index only first CONFIG_URL_MAX_CONTENT bytes of page content
$CRAWL_URL_MAX_CONTENT = 512 * 1024; // default 150 * 1024

// HACK. cooldown time after http request.
$CRAWL_THREAD_SLEEP_TIME = 100000; //mk_sec

// **** DATA STORAGE ***
$CRAWL_CHARS_PER_WORD = 3; // number of characters to use to represent a word
// 1 for really small site; 3 for really large site; 4 for super mega, etc.
// database size increases by 2^($CRAWL_CHARS_PER_WORD * 8)
// tables should be emptied and site re-crawled if this is changed

// **** SEARCH CONFIG ****

$CRAWL_RESULTS_PER_PAGE = 10;
// ******************************** IF SEARCH IS SLOW, MAKE THE BELOW ZERO (0) ******************************
$CRAWL_SEARCH_CONTENT_SIZE = 0; // the larger this integer, the larger the description of content for each result
$CRAWL_SEARCH_TEXT_SURROUNDING_LENGTH = 3; // words
$CRAWL_SEARCH_TEXT_BOLD_QUERY_WORD = true; // bold query words in results
$CRAWL_SEARCH_TEMPLATE = '<a href="%u" class="tx_blue">%t</a><br />%c<span class="tx_url">%u</span>';
// format: %u = url, %t = title, %d = last crawled date, %c = formatted result content
// default: $CRAWL_SEARCH_TEMPLATE = '<a href="%u" class="tx_blue">%t</a><br />%c<br /><span class="tx_url">%u</span>';
// deprecated: $CRAWL_SEARCH_TEMPLATE = '&#149; <a href="%u" class="tx_blue">%t</a> &#151; %c';

// These two variables are deprecated
$CRAWL_SEARCH_TEXT_SURROUNDING_LENGHT = 70; //chars;
$CRAWL_SEARCH_MAX_RES_WORD_COUNT = 2; // larger value produces larger search page

// *** INIT ****
setlocale (LC_ALL, $CRAWL_LOCALE);

}

?>
